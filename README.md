### This includes source code of the iPhone Application built for GenZe eBike. ###

The GenZe version 3.1 app is designed and developed for eBike, which connect with the eBike via the Users Smartphone. 

Bluetooth feature enables NFC communications for the data transfer between two devices. When the user smartphone is connected to the Bluetooth Device, then the user will able to use the GenZe Mobile app to receive Bluetooth data (6 data elements – Speed, mode, odometer, charge, error codes, Unit ID) that is then transmitted back to the Server.